#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "laby.h"

/** This main function is given as a sample and can
 * be changed as needed for debugging purpose
 **/
int main(int argv, char **argc) {
    if(argv != 2){
        printf("Need 2 arguments \n");
        return EXIT_FAILURE;
    }
    FILE* fp;
    fp = fopen(argc[1], "r");  // read mode - all 30 first maps available as
                                   // map01.txt to map30.t
    if (fp == NULL) {
        fprintf(stderr, "Problem opening file");
        exit(EXIT_FAILURE);
    }
    s_labymap *p_m = get_labymap_from_file(fp);
    draw_labymap(p_m);
    fclose(fp);
    if (p_m == NULL) return EXIT_FAILURE;
    e_direction * solution = resolve(p_m);
    uint i=0;
    while(*(solution+i)!=DIRUNDEFINED){
        //apply_operation(copy_p_m, *(solution + i));
        //draw_labymap(copy_p_m);
        switch(*(solution+i)){
            case UP:printf("UP\n");break;
            case DOWN:printf("DOWN\n");break;
            case LEFT:printf("LEFT\n");break;
            case RIGHT:printf("RIGHT\n");break;
            default: break;
        }
        i++;
    }
    free(p_m);
    return EXIT_SUCCESS;
}