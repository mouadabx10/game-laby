CFLAGS=-Wall -g -std=c99
all : student


student  : student.o laby.o
	$(CC) $^ -o $@

clean:
	rm -f student *.o

.PHONY: all clean