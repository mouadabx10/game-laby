#include "laby.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

char *val_to_str(int val, bool is_current){
    char text[6];
    char *output = malloc(6 * sizeof(char));
    char *format;
    if(val >= 100000) {
        return "_inf_";
    } else {
        if(val % 10 == val) {
            format = (is_current)? "  %d^ " : "  %d  ";
            goto finish;
        } else if(val % 100 == val) {
            format = (is_current)? " %d^ " : " %d  ";
            goto finish;
        } else if(val % 1000 == val) {
            format = (is_current)? " %d^" : " %d ";
            goto finish;
        } else if(val % 10000 == val) {
            format = (is_current)? "%d^" : "%d ";
            goto finish;
        } else {
            format = "%d";
            goto finish;
        }
    }
    finish:
    sprintf(text, format, val);
    strcpy(output, text);
    return output;

}

char *op_to_string(e_operation op){
    char *output = malloc(6 * sizeof(char));
    char text[6];
    sprintf(text, "  %c  ", op);
    strcpy(output, text);
    return output;
}


int get_actual_value(s_labymap* p_m) {
    if (p_m==NULL){
        fprintf(stderr,"memory error!");
        exit(EXIT_FAILURE);
    }
    return p_m->content[p_m->position].operand_value;
}

s_labymap* get_labymap_from_file(FILE* f) {
    s_labymap *labymap = malloc(sizeof(s_labymap));
    int index_line = 0;
    char * line = NULL;
    size_t len = 0;
    size_t read;
    while ((read = getline(&line, &len, f)) != -1) {
        if (index_line == 0) labymap->level_id = (uint) (atoi(line));
        else if (index_line == 1) {
            uint w = atoi(line);
            labymap->width = w;
            labymap->content = malloc((size_t)(w * w) * sizeof(u_content));
            for (int i = 0; i < (int)(w * w); i+=2) {
                labymap->content[i].operand_value = -1;
            }
        } else if(index_line == 2) {
            int count_val = 0;
            u_content current_content;
            char *sentence_dup = malloc(sizeof(char) * (read - 1));
            strcpy(sentence_dup, line);
            char *curr_word = strtok(sentence_dup, " ");
            while (curr_word != NULL) {
                if((count_val % labymap->width) % 2 == 1){
                    count_val += labymap->width - 1;
                }
                current_content.operand_value = atoi(curr_word);
                labymap->content[count_val] = current_content;
                count_val += 2;
                curr_word = strtok(NULL, " ");
            }
            free(sentence_dup);

        }
        else if (index_line == 3) {
            int count_op = 1;
            for (int i = 0; i < (int) (read - 1); ++i) {
                u_content current_content;
                current_content.operator_code = line[i];
                labymap->content[count_op] = current_content;
                count_op += 2;
            }
        }
        else if (index_line == 4) {
            labymap->position = (uint) (atoi(line));
            labymap->starting_score = labymap->content[labymap->position].operand_value;
        }
        else if (index_line == 5) labymap->goal_score = atoi(line);
        else {
            index_line++;
            continue;
        }
        index_line++;
    }
    return labymap;
}

e_operation get_operation(s_labymap* p_m, e_direction d) {
    if(p_m == NULL){
        printf("Labymap ne doit pas etre null \n");
        exit(EXIT_FAILURE);
    }
    int i, j, new_i, new_j;
    i = p_m->position / p_m->width;
    j = p_m->position % p_m->width;
    if((i == 0 && d == UP) || (i == p_m->width - 1 && d == DOWN)) return UNDEFINED;
    if((j == 0 && d == LEFT) || (j == p_m->width - 1 && d == RIGHT)) return UNDEFINED;
    new_i = (d == UP) ? i - 1 : (d == DOWN) ? i + 1 : i;
    new_j = (d == LEFT) ? j - 1 : (d == RIGHT) ? j + 1 : j;
    return p_m->content[new_i * p_m->width + new_j].operator_code;
}

e_operation opposite_operation(e_operation op){
    return (op == PLUS) ? MINUS :
           (op == MINUS) ? PLUS :
           (op == DIVIDE) ? MULTIPLY :
               (op == MULTIPLY) ? DIVIDE : UNDEFINED;
}

bool apply_operation(s_labymap* p_m, e_direction d) {
    e_operation op = get_operation(p_m, d);
    if(op == UNDEFINED){
        return false;
    }
    int operand_1 = p_m->content[p_m->position].operand_value;
    int operand_2, operand;
    int i, j, new_i, new_j;
    i = p_m->position / p_m->width;
    j = p_m->position % p_m->width;
    new_i = (d == UP) ? i - 2 : (d == DOWN) ? i + 2 : i;
    int i_op = (d == UP) ? i - 1 : (d == DOWN) ? i + 1 : i;
    new_j = (d == LEFT) ? j - 2 : (d == RIGHT) ? j + 2 : j;
    int j_op = (d == LEFT) ? j - 1 : (d == RIGHT) ? j + 1 : j;
    operand_2 = p_m->content[new_i * p_m->width + new_j].operand_value;
    if(operand_1 % operand_2 != 0 && op == DIVIDE){
        return false;
    }
    operand = (op == PLUS) ? operand_1 + operand_2 :
              (op == MINUS) ? operand_1 - operand_2 :
              (op == MULTIPLY) ? operand_1 * operand_2 :
              (op == DIVIDE ) ? operand_1 / operand_2 : -1;
    if(operand > 99 ||  0 > operand){
        return false;
    }
    p_m->position = (uint)(new_i * p_m->width + new_j);
    p_m->content[p_m->position].operand_value = operand;
    p_m->content[i * p_m->width + j].operand_value = operand_2;
    p_m->content[i_op * p_m->width + j_op].operator_code = opposite_operation(op);
    return true;
}

void draw_labymap(s_labymap* p_m){
    int index;
    if(p_m == NULL){
        printf("Labymap ne doit pas etre null \n");
        exit(EXIT_FAILURE);
    }
    /* Level <L_ID> : <S_SCORE> -> <G_SCORE> : <A_SCORE> */
    printf("Level %u : %d -> %d : %d \n",
           p_m->level_id,
           p_m->starting_score,
           p_m->goal_score,
           p_m->content[p_m->position].operand_value);

    for (int i = 0; i < p_m->width; ++i) {
        printf("[");
        for (int j = 0; j < p_m->width; ++j) {
            index = p_m->width * i + j;
            if(index % 2 == 0){
                bool is_current = index == (int)p_m->position;
                if(p_m->content[index].operand_value != -1){
                    printf("%s", val_to_str(p_m->content[index].operand_value, is_current));
                } else {
                    printf("     ");
                }
            } else {
                printf("%s", op_to_string(p_m->content[index].operator_code));
            }
        }
        printf("]\n");
    }
}

char *direction_to_string(e_direction dir){
    return (dir == UP) ? "UP" :
           (dir == DOWN) ? "DOWN" :
           (dir == LEFT) ? "LEFT" :
           (dir == RIGHT) ? "RIGHT" : "UNDEFINED";
}

e_direction opposite_dir(e_direction dir){
    return (dir == UP) ? DOWN :
           (dir == DOWN) ? UP :
           (dir == LEFT) ? RIGHT :
           (dir == RIGHT) ? LEFT : DIRUNDEFINED;
}

e_direction *other_directions(e_direction dir){
    e_direction *directions = malloc(4 * sizeof(e_direction));
    directions[0] = UP;
    directions[1] = RIGHT;
    directions[2] = DOWN;
    directions[3] = LEFT;
    if(dir == DIRUNDEFINED){
        return directions;
    }
    e_direction *other_directions = malloc(4 * sizeof(e_direction));
    for (int i = 0; i < 4; ++i) {
        if(dir != opposite_dir(directions[i])){
            other_directions[i] = directions[i];
        } else {
            other_directions[i] = DIRUNDEFINED;
        }
    }
    return other_directions;
}

bool recursive(s_labymap *labymap, e_direction previous_direction, e_direction **resolve_dir, int *count){
    bool output;
    e_direction *other_dir = other_directions(previous_direction);
    for (int i = 0; i < 3; ++i) {
        if(apply_operation(labymap, other_dir[i])){
            if(labymap->content[labymap->position].operand_value == labymap->goal_score){
                return true;
            }
            output = recursive(labymap, other_dir[i], resolve_dir, count);
            if(!output) {
                apply_operation(labymap, opposite_dir(previous_direction));
                continue;
            } else {
                printf("%s \n", direction_to_string(other_dir[i]));
            }
        }
    }
    return false;
}

bool recursive_path(s_labymap *labymap, e_direction current_direction, e_direction **path, int *count){
    e_direction *other_dir = other_directions(current_direction);
    bool boolean = false;
    for (int i = 0; i < 4; ++i) {
        if(other_dir[i] == DIRUNDEFINED) continue;
        if(apply_operation(labymap, other_dir[i])){
            if(labymap->content[labymap->position].operand_value == labymap->goal_score){
                (*count)++;
                (*path)[*count] = other_dir[i];
                return true;
            }
            boolean = recursive_path(labymap, other_dir[i], path, count);
            if(boolean){
                (*count)++;
                (*path)[*count] = other_dir[i];
                return true;
            }
            else continue;
        }
    }
    apply_operation(labymap, opposite_dir(current_direction));
    return false;
}

e_direction *resolve(s_labymap* p_m){
    e_direction *path = malloc(1000 * sizeof(e_direction));
    int count = 0;
    recursive_path(p_m, DIRUNDEFINED, &path, &count);
    e_direction *path_output = malloc((count + 1) * sizeof(e_direction));
    for (int i = 0; i < count+1; ++i) {
        memcpy(&path_output[i], &path[count - i], sizeof(e_direction));
    }
    path_output[count + 1] = DIRUNDEFINED;
    return path_output;
}

void free_labymap(s_labymap* p_m) {
    free(p_m->content);
    free(p_m);
}
