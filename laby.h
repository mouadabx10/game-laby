#ifndef _LABY_H_
#define _LABY_H_
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>


typedef unsigned int uint;

typedef union {
  int operand_value;
  char operator_code;
} u_content;

// The labyMap structure to be used
typedef struct {
  uint level_id;       // identifier of the level
  uint width;          // number of cells (counting empty ones)
  u_content* content;  // array of the u_content defining numerical values or
                       // operator code in order from top left to down right
  uint position;       // current position
  int starting_score;  // score of the starting position
  int goal_score;      // target score
} s_labymap;

// Constant values to be used
typedef enum { UP, DOWN, RIGHT, LEFT, DIRUNDEFINED } e_direction;
typedef enum {
  PLUS = '+',
  MINUS = '-',
  MULTIPLY = '*',
  DIVIDE = '/',
  UNDEFINED = '?'
} e_operation;

/*!
 * This function transforms a number to a string with 5 characters
 * for example : 1 is transformed to "__1__" and 234 to "_234_" where _ is considered as space character
 * @param is_current determines if the value is in the current position in the labymap
 */
char *val_to_str(int val, bool is_current);

/*!
 * This function transforms an operation code to a string with 5 characters
 * for example : + is transformed to "  +  "
 */
char *op_to_string(e_operation op);

/*!
 * This function returns the actual score of a labyMap.
 *
 * \param p_m a pointer on a s_labymap.
 * \return the actual score corresponding to the content of the
 *  actual cell.
 */
int get_actual_value(s_labymap* p_m);

/*!
 * This function loads a labyMap from a file containing information
 * in successive lines.
 * 1st line contains the level id.
 * 2nd line contains the width of the map.
 * 3rd line contains numerical content of the cells of the map from
 * up left to down right as a list of integers separated by spaces.
 * 4th line contains the content of the operand cells of the map from
 * up left to down right as a string composed of '+', '-', '*', '/'
 * characters.
 * 5th line contains the starting position as an index position in
 * the content (not coordinates).
 * 6th line contains the goal score.
 * Note that the starting position also defines the starting score.
 *
 * \param f a File to read informations from.
 * \return the adress of a labyMap allocated in the heap or NULL in
 * case of error.
 */
s_labymap* get_labymap_from_file(FILE* f);

/*!
 * This function returns the actual operation in a given direction
 * of the actual position in a labyMap.
 *
 * \param p_m a pointer on a labyMap.
 * \param d a direction.
 * \return the operation in the direction d of the actual position; UNDEFINED
 * over invalid parameter.
 */
e_operation get_operation(s_labymap* p_m, e_direction d);

/*!
 * This function modifies a labyMap by applying the operation in the
 * direction d of the actual position.
 *
 * \param p_m a pointer on a labyMap.
 * \param d a direction.
 * \return true if the operation was successfull; false otherwise.
 */
bool apply_operation(s_labymap* p_m, e_direction d);

/*! This function draw an ASCII version of a labyMap as follows.
 * Level <L_ID> : <S_SCORE> -> <G_SCORE> : <A_SCORE>
 * [<C00>_<C01>_<C02>_<C03>_<C04>_]
 * [<C05>_<GAP>_<C07>_<GAP>_<C09>_]
 * [<C10>_<C11>_<C12>_<C13>_<C14>_]
 * [<C15>_<GAP>_<C17>_<GAP>_<C19>_]
 * [<C20>_<C21>_<C22>_<C23>_<C24>_]
 *
 * where <L_ID>, <S_SCORE> and <G_SCORE> are replaced by the level
 * id, starting and goal scores of the labyMap and <A_SCORE> by the
 * actual score.
 * where <CXX> corresponds to the content of the XXth cell of the
 * labyMap printed as a 5 characters string and <GAP> are empty
 * strings of 5 spaces.
 * The current position is denoted by a ^ character appearing just
 * after the actual value instead of the _ (representing a real space)
 * when a value needs more than 5 digits, a symbol _inf_ is printed
 *
 * \param p_m a pointer on a labyMap to be displayed.
 * On unvalid pointer the function does not draw anything
 */
void draw_labymap(s_labymap* p_m);

/*!
 * This function returns a path solving a given labyMap.
 *
 * \param p_m a pointer on a labyMap to be solved.
 * \return a path (an array of directions) corresponding to a
 * solution of the laby that can be tested on the laby website
 * http://laby.thr.pm/
 * By convention, the array should end with a DIRUNDEFINED direction
 */

/*!
 * This function returns the direction as a string
 * for example : UP is returned as "UP"
 */
char *direction_to_string(e_direction dir);

/*!
 * This function returns the opposite of a direction
 * for example : the opposite of UP is DOWN
 * @param dir
 * @return
 */
e_direction opposite_dir(e_direction dir);

/*!
 * This function returns an array of 4 directions except the opposite direction
 * The opposite direction is replaced with DIRUNDEFINED direction
 * for example : for UP the array is [UP, RIGHT, DIRUNDEFINED, LEFT]
 */
e_direction *other_directions(e_direction dir);

/*!
 * This function browse all possible directions starting from current_direction
 * and check if the current score is equal to the goal score
 * @param labymap : a pointer to the map
 * @param current_direction : the direction applied before browsing possible directions
 * @param path : pointer to an array of directions where to store the path to the solution of the map
 * @param count : address to the variable where to store the number of directions defining the path
 * @return
 */
bool recursive_path(s_labymap *labymap, e_direction current_direction, e_direction **path, int *count);

/*!
 * This function resolves the labymap using the recursive function and returns an array
 * of directions defining a path to the solution of the labymap
 * @param p_m : a pointer to the labymap
 */
e_direction *resolve(s_labymap *p_m);

/*!
 * This function frees all the memory allocations used previously to create the map
 * @param p_m : a pointer to the labymap
 */
void free_labymap(s_labymap* p_m);



#endif